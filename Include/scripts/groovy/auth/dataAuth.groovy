package auth

import com.kms.katalon.core.testobject.ResponseObject

class dataAuth {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	private ResponseObject response

	public ResponseObject getResponse() {
		return response
	}

	public void setResponse(ResponseObject response) {
		this.response = response
	}
}