package auth
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class register {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	def data = new auth.dataAuth()

	@When("I send register request")
	def I_register() {
		def response = WebUI.callTestCase(findTestCase('pages/auth/register'), [:], FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@Then("I verify to register")
	def I_verify_register() {
		def response = data.getResponse()
		WebUI.callTestCase(findTestCase('pages/auth/register-assert'), [('response'):response], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I send null email request")
	def I_send_null_email_request() {
		def response = WebUI.callTestCase(findTestCase('pages/auth/register - emailNull'), [:], FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@Then("I verify email error message")
	def I_verify_email_error_message() {
		def response = data.getResponse()
		WebUI.callTestCase(findTestCase('pages/auth/register-assert-error'), [('response'):response], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I send null password request")
	def I_send_null_password_request() {
		def response = WebUI.callTestCase(findTestCase('pages/auth/register - passwordNull'), [:], FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@Then("I verify password error message")
	def I_verify_password_error_message() {
		def response = data.getResponse()
		WebUI.callTestCase(findTestCase('pages/auth/register-assert-error'), [('response'):response], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I send null name request")
	def I_send_null_name_request() {
		def response = WebUI.callTestCase(findTestCase('pages/auth/register - nameNull'), [:], FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@Then("I verify name error message")
	def I_verify_name_error_message() {
		def response = data.getResponse()
		WebUI.callTestCase(findTestCase('pages/auth/register-assert-error'), [('response'):response], FailureHandling.STOP_ON_FAILURE)
	}
}