package product
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class productScript {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	def data = new product.dataProduct()

	@When("I send request to create product")
	def I_want_to_create_product() {
		WebUI.callTestCase(findTestCase('pages/product/create'),[:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify the product created")
	def I_want_to_veryfy_product() {
	}

	//todo to the next scenario
	@When("I send request to create comment product")
	def I_want_to_create_comment_product() {
		// Implement your logic to send a request to create comment product
		WebUI.callTestCase(findTestCase('pages/product/create-comment'),[:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify the product comment")
	def I_want_to_verify_product_comment() {
		// Implement your verification logic here
	}

	@When("I send request to create rating product")
	def I_want_to_create_rating_product() {
		// Implement your logic to send a request to create rating product
		WebUI.callTestCase(findTestCase('pages/product/create-rating'),[:], FailureHandling.STOP_ON_FAILURE)

	}

	@Then("I verify the product rating")
	def I_want_to_verify_product_rating() {
		// Implement your verification logic here
	}

	@When("I send request to get product")
	def I_want_to_get_product() {
		// Implement your logic to send a request to get product
		def response = WebUI.callTestCase(findTestCase('pages/product/get-product'),[:], FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@Then("I verify the product")
	def I_want_to_verify_the_product() {
		// Implement your verification logic here
		def response = data.getResponse()
		WebUI.callTestCase(findTestCase('pages/product/get-product-assert'),[('response'):response], FailureHandling.STOP_ON_FAILURE)

	}

	@When("I send request to get all product")
	def I_want_to_get_all_product() {
		// Implement your logic to send a request to get all product
		def response = WebUI.callTestCase(findTestCase('pages/product/get-all-product'),[:], FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@Then("I verify list of product")
	def I_want_to_verify_list_of_product() {
		// Implement your verification logic here
		def response = data.getResponse()
		WebUI.callTestCase(findTestCase('pages/product/get-all-product-assert'),[:], FailureHandling.STOP_ON_FAILURE)

	}

	@When("I send request to get product comment")
	def I_want_to_get_product_comment() {
		// Implement your logic to send a request to get product comment
		def response = WebUI.callTestCase(findTestCase('pages/product/get-product-comment'),[:], FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}


	@Then("I verify the comment")
	def I_want_to_verify_comment() {
		// Implement your verification logic here
		def response = data.getResponse()
		WebUI.callTestCase(findTestCase('pages/product/get-product-comment-assert'),[:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I send request to get product rating")
	def I_want_to_get_product_rating() {
		// Implement your logic to send a request to get product rating
		def response = WebUI.callTestCase(findTestCase('pages/product/get-product-rating'),[:], FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@Then("I verify the rating")
	def I_want_to_verify_rating() {
		// Implement your verification logic here
		def response = data.getResponse()
		WebUI.callTestCase(findTestCase('pages/product/get-product-rating-assert'),[('response'):response], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I send request to delete a product")
	def I_want_to_delete_a_product() {
		// Implement your logic to send a request to delete a product
		WebUI.callTestCase(findTestCase('pages/product/delete'),[:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify the deleted product")
	def I_want_to_verify_deleted_product() {
		// Implement your verification logic here
	}
}