package product

import com.kms.katalon.core.testobject.ResponseObject

class dataProduct {
	private ResponseObject response

	public ResponseObject getResponse() {
		return response
	}

	public void setResponse(ResponseObject response) {
		this.response = response
	}
}