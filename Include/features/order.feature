#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@order
Feature: Order
  After login, I wanna order some item and getting info about order

  @order-create
  Scenario: As a user, i want to buy some item
    When I send order request
    Then I verify my order
    
  @order-get-all
  Scenario: I want to get all info about order
  	When I send get all info order request
  	Then I verify all order info
  	
  @order-get-by-id
  Scenario: I want to get info about order by id
  	When I send get order by id
  	Then I verify get order by id

