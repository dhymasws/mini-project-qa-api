#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@product
Feature: product
  I want to use this template for my feature file

  @product-create
  Scenario: I want to create product
    When I send request to create product
    Then I verify the product created
    
    
  @product-create-comment
  Scenario: I want to create comment product
  	When I send request to create comment product
  	Then I verify the product comment 
  
  @product-create-rating
  Scenario: I want to create rating product
  	When I send request to create rating product
  	Then I verify the product rating
  @product-get
  Scenario: I want to get product
  	When I send request to get product
  	Then I verify the product
  @product-getAll
  Scenario: I want to get all product
  	When I send request to get all product
  	Then I verify list of product
  @product-get-comment
  Scenario: I want to get product comment
  	When I send request to get product comment
  	Then I verify the comment
  @product-get-rating
  Scenario: I want to get product rating
  	When I send request to get product rating
  	Then I verify the rating
  
  @product-delete
  Scenario: I want to delete product
  	When I send request to delete a product
  	Then I verify the deleted product


