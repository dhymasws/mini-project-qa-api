
@prod_cat
Feature: product category
  After login, I wanna add some category and getting info it

  @cat-create
  Scenario: I want add category
    When I send create category request
    Then I verify my category 
    
  @cat-get-all
  Scenario: I want to get all info about category
  	When I send get all info category request
  	Then I verify all category info
  	
  @cat-get-by-id
  Scenario: I want to get info about category by id
  	When I send get category by id
  	Then I verify get category by id
  	
  @cat-delete
  Scenario: I want delete category by id
  	When I send delete category request by id
  	Then I verify categry was deleted