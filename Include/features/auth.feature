#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@auth
Feature: login
  I want login with my account

  @auth-login
  Scenario: I want login with my account
    When 	I send login request 
    Then  I verify to login
    
  @auth-login-failure-pass
  Scenario: I want login with wrong pass
  	When I send wrong pass
  	Then I get pass error message
  	
  @auth-login-failure-email
  Scenario: I want to login with an email that has never been registered
  	When I send an login request with new email
  	Then I get email error message
  
  
  @auth-register
  Scenario: I want to register
   	When I send register request
   	Then I verify to register
   	
  @auth-register-email-null
  Scenario: I want to registering with null email
  	When I send null email request
  	Then I verify email error message
  
  
  @auth-register-password-null
  Scenario: I want to registering with null email
  	When I send null password request
  	Then I verify password error message
  
  @auth-register-name-null
  Scenario: I want to registering with null email
  	When I send null email request
  	Then I verify name error message
  
  @auth-get-user-info
  Scenario: After login,  i want to see my profile account
  	When I send check info request
  	Then I see my profile
  	
  

